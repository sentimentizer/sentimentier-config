#!/bin/bash

echo "Creating rows for SentimentizerTopicDistribution for topic: "$1
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.1"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'

aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.2"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'

aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.3"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'

aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.4"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'

aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.5"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'

aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.6"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'

aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.7"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'

aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.8"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'

aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.9"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.10"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'



aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.11"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.12"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.13"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.14"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.15"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.16"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.17"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.18"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.19"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "-5"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "-4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "-3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "-2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "-1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "0"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "1"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "2"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "3"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "4"},"totalVotes": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicDistribution --item '{"topic": {"S": "'$1'.20"},"voteIncrement": {"N": "5"},"totalVotes": {"N": "0"}}'


echo "Rows created."
echo "Create row for SentimentizerTopicTotals for topic: "$1
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.1"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.2"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.3"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.4"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.5"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.6"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.7"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.8"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.9"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.10"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.11"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.12"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.13"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.14"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.15"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.16"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.17"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.18"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.19"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."
aws dynamodb put-item --table-name SentimentizerTopicTotals --item '{"topic": {"S": "'$1'.20"},"sentimentVotes": {"N": "0"}, "sentimentTotalNegative": {"N": "0"}, "sentimentTotalPositive": {"N": "0"}}'
echo "."

echo "Rows created."

echo "Create row for SentimentizerTopicStats for topic: "$1
aws dynamodb put-item --table-name SentimentizerTopicStats --item '{"topic": {"S": "'$1'.1"},"votesPerMinute": {"N": "0"},"sentimentVotes": {"N": "0"}}'
echo "Row created."
